var n = 10000, array = [], sortedNative, sortedNSquare, sortedNlogN;


console.time('native sorting');
// place native sorting here
console.timeEnd('native sorting');


console.time('n-square sorting');
/* HERE SHOULD BE NAME OF ALGORITHM */
// place n-square sorting here
console.timeEnd('n-square sorting');


console.time('n-log-n sorting');
/* HERE SHOULD BE NAME OF ALGORITHM */
// place n log n sorting here
console.timeEnd('n-log-n sorting');


// checking
function checker(array, name) {
    for (var i = 0; i < n - 2; i++) {
        if (array[i] > array[i + 1]) {
            throw 'Something went wrong with ' + name;
        }
    }
}

checker(sortedNative, 'native sorting');
checker(sortedNSquare, 'n-square sorting');
checker(sortedNlogN, 'n-log-n sorting');
